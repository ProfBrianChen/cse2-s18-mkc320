//////////
////CSE 2
///Megan Carroll 2/4/18
//creating code that calculates tax on different items and their costs 
public class Arithmetic {
  
    public static void main (String[] args) {
      int numPants = 3;  //number of pairs of pants
      double pantsPrice = 34.98; //cost per pair of pants 
      int numShirts = 2; //number of sweatshirts
      double shirtPrice = 24.99; //cost per shirt
      int numBelts = 1; //number of belts
      double beltCost = 33.99; //cost per belt 
      double paSalesTax = 0.06; //the tax rate
      double totalCostOfPants;
      double totalCostOfShirts;
      double totalCostOfBelts;
      //calculations for total cost of items
      totalCostOfPants=numPants*pantsPrice;
      totalCostOfShirts=numShirts*shirtPrice;
      totalCostOfBelts=numBelts*beltCost;
      
      double salesTaxChargedOnPants;
      double salesTaxChargedOnShirts;
      double salesTaxChargedOnBelts;
      
      //calculations for cost of sales tax on item type
      salesTaxChargedOnPants=numPants*pantsPrice*paSalesTax;
      salesTaxChargedOnShirts=numShirts*shirtPrice*paSalesTax;
      salesTaxChargedOnBelts=numBelts*beltCost*paSalesTax;
      
      double totalCostOfPurchases; //before tax 
      totalCostOfPurchases=totalCostOfPants+totalCostOfShirts+totalCostOfBelts;
      double totalSalesTax;
      totalSalesTax=salesTaxChargedOnPants+salesTaxChargedOnShirts+salesTaxChargedOnBelts;
      double totalPaidForTransaction; //including sales tax 
      totalPaidForTransaction=totalCostOfPurchases+totalSalesTax;
      
      //following is to truncate the values with larger decimals to vales with two decimal places
      int taxPants = (int) (salesTaxChargedOnPants*100); 
      salesTaxChargedOnPants=taxPants/100.0;
      int taxShirts = (int) (salesTaxChargedOnShirts*100);
      salesTaxChargedOnShirts = taxShirts/100.0;
      int taxBelts = (int) (salesTaxChargedOnBelts*100);
      salesTaxChargedOnBelts = taxBelts/100.0;
      int totalTax = (int) (totalSalesTax*100);
      totalSalesTax = totalTax/100.0;
      int totalCostWithTax = (int) (totalPaidForTransaction*100);
      totalPaidForTransaction = totalCostWithTax/100.0;
        
      System.out.println("Purchasing all the Pants costs " + (totalCostOfPants) + " dollars"); 
      System.out.println("Purchasing all the Shirts costs " + (totalCostOfShirts) + " dollars");
      System.out.println("Purchasing all the Belts costs " + (totalCostOfBelts) + " dollars");
      System.out.println("Total sales tax for buying all Pants is " + (salesTaxChargedOnPants) + " dollars");
      System.out.println("Total sales tax for buying all Shirts is " + (salesTaxChargedOnShirts) + " dollars");
      System.out.println("Total sales tax for buying all Belts is " + (salesTaxChargedOnBelts) + " dollars");
      System.out.println("Total cost of purchases before tax is " + (totalCostOfPurchases) + " dollars");
      System.out.println("Total sales tax is " + (totalSalesTax) + " dollars");
      System.out.println("Total cost of purchases including tax is " + (totalPaidForTransaction) + " dollars");
        
      
    
    }
}