import java.util.Random;
import java.util.Arrays;
public class DrawPoker{
  
  
  public static int[] copy(int[] deck0){
    int[] copy = new int [deck0.length];
    for(int i=0; i < deck0.length; i++){
      copy[i] = deck0[i];
      //System.out.print(copy[i] + " ");
    }
    //System.out.println();
    System.out.println("Deck copied");
    return copy;
  }
  //////////////////////////////////////////////////////// the following shuffles the array
  public static void shuffleArray(int[] deck1){
    int j = deck1.length;
    Random random = new Random();
    random.nextInt();
    for(int i = 0; i<j; i++){
      int change = i + random.nextInt(j-i);
      swap(deck1 , i, change);
    }
  }
  
  public static void swap(int[] deck1, int i, int change){
    int helper = deck1[i];
    deck1[i] = deck1[change];
    deck1[change] = helper;
  }
  
  
  ////////////////////////////////////////////////////////
  
 /* 
  public static boolean isPairPlayer1(int[] player1){
    int count = 0;
    for(int i=0; i < player1.length; i++){
      for(int j=0; j<i; j++){
        if(player1[i] .equals(player1[j])) count++;
      }
      if(count == 1) return true;
      count = 0;
    }
    return false;
  }
  
  public static boolean isPairPlayer2(int[] player2){
    int count = 0;
    for(int i=0; i < player2.length; i++){
      for(int j=0; j<i; j++){
        if(player2[i] .equals(player2[j])) count++;
      }
      if(count == 1) return true;
      count = 0;
    }
    return false;
  }
  */
  //////////////////////////////////////////////
  
  
  public static void main(String[]Args){
    int[] deck0 = new int [52];
    for(int i=0; i<deck0.length;i++){
      deck0[i] = i;
      //System.out.print(deck0[i] + " ");
    }
    //System.out.println();
    
    System.out.println("Original deck of cards:" + Arrays.toString(deck0));// to check that the original deck is properly allocated numbers 0-51
    
    int[] deck1 = copy(deck0);
    shuffleArray(deck1);
    for (int i : deck1){
      //System.out.print(i + " ");
    }
    //System.out.println();    
    System.out.println("Shuffled deck of cards:" + Arrays.toString(deck1)); // to check the contents of the array matches the suffle
    int[] player1 = new int[5];
    int[] player2 = new int[5]; // creating arrays that represent each players deck of cards
    int k = 0;
    for(int j=0; j<10; j++){
      if(j%2 ==0){
        player1[k] = deck1[j];
      }
      else{
       player2[k] = deck1[j];
        k++;
      }
    }
    System.out.println("Player 1's cards: " + Arrays.toString(player1));
    System.out.println("Player 2's cards: " + Arrays.toString(player2));
   // int shuffledDeck = ;
   // System.out.print("copied deck " + shuffledDeck + " ");
   // System.out.println();
   
    /*
    boolean pair1 = isPairPlayer1(player1);
    boolean pair2 = isPairPlayer2(player2);
    System.out.println("Player one has a pair: " + pair1);
    System.out.println("Player two has a pair: " + pair2);
    */
    
  }
  
}