import java.util.Scanner;
public class Area{
  
  public static void rectangle(int X, int Y) {
    Scanner myScanner = new Scanner(System.in);
    System.out.print("Enter shape length: ");
    int length = myScanner.nextInt();
    System.out.print("Enter shape height: ");
    int height = myScanner.nextInt();
    int area = length*height;
    System.out.println("Rectangle area " + area);
    
  }
  public static void triangle(int x, int y){
    Scanner myScanner = new Scanner(System.in);
    System.out.print("Enter triangle base length: ");
    int length = myScanner.nextInt();
    System.out.print("Enter triangle height: ");
    int height = myScanner.nextInt();
    int area = (length*height)/2; 
    System.out.println("Triangle area " + area);
    
  }
 
  public static void circle(int x, int y){
    Scanner myScanner = new Scanner(System.in);
    System.out.print("Enter circle radius: ");
    int radius = myScanner.nextInt();
    double area = 3.1415*(radius*radius);
    System.out.println("Circle area " + area);
    
  }
  public static void main (String [] args) {
    Scanner myScanner = new Scanner(System.in);
    System.out.print("Choose shape rectangle, triangle, or circle: "); 
  
    
    //
    if (myScanner.next().equals("rectangle")){
      int x=1, y=1;
      System.out.println("Sent to rectangle method"); 
      rectangle(x, y);
    }
    //
    if (myScanner.next().equals("triangle")){
      int x=1, y=1;
      System.out.println("Sent to triangle method"); 
      triangle(x, y);
    }
    //
    if (myScanner.next().equals("circle")){
      int x=1, y=1;
      System.out.println("Sent to circle method"); 
      circle(x, y);
    }  
    if (!myScanner.next().equals("rectangle" + "triangle" + "circle")){
      System.out.print("Enter shape with no capitals: ");
    }
    
    return;
      /*while(!myScanner.hasNext()){
      *String junk = myScanner.next();
      System.out.print("Enter a String: ");
      myScanner.next(); */
    
  }
  
}