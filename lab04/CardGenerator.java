/////////
////CSE02 Megan Carroll
///2/16/18
///creating a Card Generator
//
public class CardGenerator{
    public static void main(String []args) {
      String suit = " ";
      String number = " ";
      int random = (int)((Math.random()*52)+1);
      System.out.println(random);
      // if and else if statements used to find the suit 
      if(random <=13){ 
        System.out.println("You picked diamond");
      }
      else if(14<= random && random <=26){
        System.out.println("You picked clubs");
      }
      else if(27<= random && random <=39){
        System.out.println("You picked hearts");
      }
      else if(40<= random && random <=52){
        System.out.println("You picked spades");
      }
      //switch statements used to tell number
      switch (random){ 
        case 1:
        case 14:
        case 27:
        case 40:
          System.out.println("ace");
          break;
        case 2:
        case 15:
        case 28:
        case 41:
          System.out.println("2");
          break;
        case 3:
        case 16:
        case 29:
        case 42:
          System.out.println("3");
          break;
        case 4:
        case 17:
        case 30:
        case 43:
          System.out.println("4");
          break;
        case 5:
        case 18:
        case 31:
        case 44:
          System.out.println("5");
          break;
        case 6:
        case 19:
        case 32:
        case 45:
          System.out.println("6");
          break;
        case 7:
        case 20:
        case 33:
        case 46:
          System.out.println("7");
          break;
        case 8:
        case 21:
        case 34:
        case 47:
          System.out.println("8");
          break;
        case 9:
        case 22:
        case 35:
        case 48:
          System.out.println("9");
          break;
        case 10:
        case 23:
        case 36:
        case 49:
          System.out.println("10");
          break;
        case 11:
        case 24:
        case 37:
        case 50:
          System.out.println("Jack");
          break;
        case 12:
        case 25:
        case 38:
        case 51:
          System.out.println("Queen");
          break;
        case 13:
        case 26:
        case 39:
        case 52:
          System.out.println("King");
          break;
        
      }
    }
}