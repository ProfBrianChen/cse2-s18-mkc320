public class RowAndColumn{
  public static int[][] increasingMatrix(int width, int height, boolean format){
    if (format==true){
      int[][] rowMajor = new int[height][width];
      int counter = 1;
      for(int i=0; i<rowMajor.length; i++){
        for(int j=0; j<rowMajor[0].length; j++){
          rowMajor[i][j] = counter;
          counter++;
        }
      }
      return rowMajor;
    }
   else{
     int[][] columnMajor = new int[width][height];
     int counter = 1;
     for(int i=0; i<width; i++){
       for(int j=0; j<height; j++){
         columnMajor[i][j] = 1+i+(width*j);
       }
     }
     /*for(int j=0; j<columnMajor[0].length; j++){
       for(int i=0; i<columnMajor.length; i++){
         columnMajor[i][j] = counter;
         counter++;
       }
     }*/
     return columnMajor;
   }
  }
  
  public static void printMatrix(int [][] array, boolean format){
    if(array==null){
      System.out.println("the array was empty!");
    }
 
    //if (format==true){
      for(int i=0; i<array.length; i++){
        for(int j=0; j<array[0].length; j++){
          System.out.printf("% 4d ", array[i][j]);
        }
        System.out.println();
      }
      System.out.println();
    //}
//     else{
//       for(int i=0; i<array.length; i++){
//         for(int j=0; j<array[0].length; j++){
//           System.out.printf("% 4d ", array[i][j]);
//         }
//         System.out.println();
//       }
//       System.out.println();
//     }
  }
  
  public static int[][] translate(int [][] array){
    int [][] translated = new int[array.length][array[0].length];
    for(int i=0; i<array.length; i++){
        for(int j=0; j<array[0].length; j++){
          translated[j][i]=array[i][j];
        } 
    }
    return translated;
  }
  
  public static int[][] addMatrix(int[][] a, boolean formata, int[][] b, boolean formatb){
    //////////
    if (formata==false){
       a = translate(a);
    }
    if (formatb==false){
       b = translate(b);
    }
    ///////
    if(a.length==b.length && a[0].length ==b[0].length){ 
      int [][] addedArray = new int[a.length][a[0].length];
      for(int i=0; i<a.length; i++){
        for(int j=0; j<a[0].length; j++){
          addedArray[i][j] = a[i][j] + b[i][j];
        }
      }
      return addedArray;
    }
    else{
      System.out.println("Arrays cannot be added");
      return null;
    }
  }
  
  public static void main (String[]Args){
    int width1 = (int) (Math.random()*10+1);
    int width2 = (int) (Math.random()*10+1);
    int height1 = (int) (Math.random()*10+1);
    int height2 = (int) (Math.random()*10+1);
    boolean format1 = true;
    boolean format2 = false;
    int [][] a = increasingMatrix(width1, height1, format1);
    int [][] b = increasingMatrix(width1, height1, format2);
    int [][] c = increasingMatrix(width2, height2, format1);
    printMatrix(a, format1);
    printMatrix(b, format2);
    printMatrix(c, format1);
    int [][] addedArray1 = addMatrix(a, format1, b, format2);
    printMatrix(addedArray1, format1);
    int[][] addedArray2 = addMatrix(a, format1, c, format1);
   
  }
}