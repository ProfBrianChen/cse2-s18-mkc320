import java.util.Scanner;
public class encrypted_x{
  
  public static void main(String [] Args){
    Scanner myScanner = new Scanner(System.in);
    System.out.println("Enter an integer between 0 and 100: ");
    int input = myScanner.nextInt();
    do {
      if(input <0 || input > 100){
      System.out.println("Input out of range! Enter value between 0 and 100: "); 
      input = myScanner.nextInt();
      }
    } while(input<0 || input > 100);
    
    System.out.println("Input: " + input);
    
    for(int i = 0; i<input; i++){
      for(int j = 0; j<input+1; j++){
        if(i==j || j==input-i){
          System.out.print(" ");
        }else{
          System.out.print("*");
        }
      }
      System.out.println();
    }
    
  } 
}