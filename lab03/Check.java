/////////////////////
//////CSE02 Megan Carroll
////2/9/18
///creating a check calculator
//
import java.util.Scanner;
public class Check{ 
      // main method required
    public static void main(String []args) {
      Scanner myScanner = new Scanner( System.in );
      System.out.print("Enter the original cost of the check in the form xx.xx: "); 
      double checkCost = myScanner.nextDouble();
      System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the form xx) :");
      double tipPercent = myScanner.nextDouble ();
      tipPercent /= 100; //to convert the percentage into a decimal value
      System.out.print("Enter the number of people that went out to dinner: ");
      double numPeople = myScanner.nextDouble ();
      double totalCost;
      double costPerPerson;
      int dollars, dimes, pennies; //dollars for whole dollar amount and dimes and pennies for values to the right of decimal point
      totalCost = checkCost * (1 + tipPercent);
      costPerPerson = totalCost / numPeople;
      //get the whole amount, dropping decimal fraction
      dollars=(int)costPerPerson;
      //get dimes amount, e.g.,
      // (int)(6.73 * 10) % 10 -> 67 % 10 -> 7
      //  where the % (mod) operator returns the remainder
      //  after the division:   583%100 -> 83, 27%5 -> 2 
      dimes=(int)(costPerPerson * 10) % 10;
      pennies=(int)(costPerPerson * 100) % 10;
      System.out.println("Each person in the group owes $" + dollars + '.' + dimes + pennies);




       


      }// end of main method 
}