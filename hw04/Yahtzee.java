/////////
/////Megan Carroll
////CSE 02 2/19/18
///
import java.util.Scanner;
public class Yahtzee{
  
    public static void main(String[] args) {
      Scanner myScanner = new Scanner(System.in); // for inputting values 
      System.out.print("Type R for a random number or type N to input your own values ");
      String rOrN= myScanner.next();
      int dice1=0;
      int dice2=0;
      int dice3=0;
      int dice4=0; 
      int dice5=0;
      // code to give random numbers when one chooses R
      if ( rOrN.equals("R")){
        dice1 = (int)((Math.random()*6)+1);
        System.out.println("Die 1 value: " + dice1);
        dice2 = (int)((Math.random()*6)+1);
        System.out.println("Die 2 value: " + dice2);
        dice3 = (int)((Math.random()*6)+1);
        System.out.println("Die 3 value: " + dice3);
        dice4 = (int)((Math.random()*6)+1);
        System.out.println("Die 4 value: " + dice4);
        dice5 = (int)((Math.random()*6)+1);
        System.out.println("Die 5 value: " + dice5);
        
      } 
      //code to let people enter their own values after choosing N
      else if ( rOrN.equals("N")){
        System.out.print("Enter value of first die ");
        int die1 = myScanner.nextInt(); //scanner being used to enter their own die integer value
        System.out.print("Enter value of Second die ");
        int die2 = myScanner.nextInt();
        System.out.print("Enter value of third die ");
        int die3 = myScanner.nextInt();
        System.out.print("Enter value of fourth die ");
        int die4 = myScanner.nextInt();
        System.out.print("Enter value of fifth die ");
        int die5 = myScanner.nextInt();
        // following if statements used when user enters a value other than 1 through 6 inclusive. Error message pops up and user is exited from program.  
          if (die1 >6 || die1<1){
            System.out.println("Error number must be between 1 and 6");
            return;
          }
        
          else if (die2 >6 || die2<1){
            System.out.println("Error number must be between 1 and 6");
            return;
          }
        
          else if (die3 >6 || die3<1){
            System.out.println("Error number must be between 1 and 6");
            return;
          }
        
          else if (die4 >6 || die4<1){
            System.out.println("Error number must be between 1 and 6");
            return;
          }
        
          else if (die5 >6 || die5<1){
            System.out.println("Error number must be between 1 and 6");
            return;
          }
          else {
            dice1 = die1;
            dice2 = die2;
            dice3 = die3;
            dice4 = die4;
            dice5 = die5;
          }
        System.out.println("Die 1 value: " + dice1);
        System.out.println("Die 2 value: " + dice2);
        System.out.println("Die 3 value: " + dice3);
        System.out.println("Die 4 value: " + dice4);
        System.out.println("Die 5 value: " + dice5);
      } 
    
      // Upper Section
      // declaring and assigning integers outside of if statements
      int aces = 0;
      int twos = 0;
      int threes = 0;
      int fours = 0;
      int fives = 0;
      int sixes = 0;
      //switch statements used to calculate number of different roll options for each die
      switch (dice1){
        case 1:
          aces += dice1;
          break;
        case 2:
          twos += dice1;
          break;
        case 3:
          threes += dice1;
          break;
        case 4:
          fours += dice1;
          break;
        case 5:
          fives += dice1;
          break;
        case 6:
          sixes += dice1;
          break;
          
      }
      switch (dice2){
        case 1:
          aces += dice2;
          break;
        case 2:
          twos += dice2;
          break;
        case 3:
          threes += dice2;
          break;
        case 4:
          fours += dice2;
          break;
        case 5:
          fives += dice2;
          break;
        case 6:
          sixes += dice2;
          break;
      }
      switch (dice3){
        case 1:
          aces += dice3;
          break;
        case 2:
          twos += dice3;
          break;
        case 3:
          threes += dice3;
          break;
        case 4:
          fours += dice3;
          break;
        case 5:
          fives += dice3;
          break;
        case 6: 
          sixes += dice3;
          break; 
      }
      switch (dice4){
          case 1:
          aces += dice4;
          break;
        case 2:
          twos += dice4;
          break;
        case 3:
          threes += dice4;
          break;
        case 4:
          fours += dice4;
          break;
        case 5:
          fives += dice4;
          break;
        case 6:
          sixes += dice4;
          break;
      }
      switch (dice5){
        case 1:
          aces += dice5;
          break;
        case 2:
          twos += dice5;
          break;
        case 3:
          threes += dice5;
          break;
        case 4:
          fours += dice5;
          break;
        case 5:
          fives += dice5;
          break;
        case 6:
          sixes += dice5;
          break;
      }
      //prints values of accounted for roll options
      System.out.println("Aces = " + aces);
      System.out.println("Twos = " + twos);
      System.out.println("Threes = " + threes);
      System.out.println("Fours = " + fours);
      System.out.println("Fives = " + fives);
      System.out.println("Sixes = " + sixes);
      
      //Upper section intitial total and bonus total  
      int upperSectionInitialTotal = aces+twos+threes+fours+fives+sixes;
      int upperSectionTotalPlusBonus = upperSectionInitialTotal;
      System.out.println("Upper Section Initial Total: " + upperSectionInitialTotal);
      if (upperSectionInitialTotal >= 63){
        upperSectionInitialTotal +=35;
      }
      System.out.println("Upper Section Total Plus Bonus: " + upperSectionTotalPlusBonus);
      
      //lower section
      boolean twoOfKind = false;
      boolean threeOfKind = false;
      boolean fourOfKind = false;
      boolean fiveOfKind = false;
      // switch statements used to find if we rolled different number of items fitting our criteria ie. four of a kind, full house, etc.
      switch (aces){
        case 2:
          twoOfKind = true;
          break;
        case 3:
          threeOfKind = true;
          break;
        case 4:
          fourOfKind = true;
          break;
        case 5:
          fiveOfKind = true;
          break;          
      }
      switch (twos){
        case 4:
          twoOfKind = true;
          break;
        case 6:
          threeOfKind = true;
          break;
        case 8:
          fourOfKind = true;
          break;
        case 10:
          fiveOfKind = true;
          break;
      }
      switch (threes){
        case 6:
          twoOfKind = true;
          break;
        case 9:
          threeOfKind = true;
          break;
        case 12:
          fourOfKind = true;
          break;
        case 15:
          fiveOfKind = true;
          break;
      } 
      switch (fours){
        case 8:
          twoOfKind = true;
          break;
        case 12:
          threeOfKind = true;
          break;
        case 16:
          fourOfKind = true;
          break;
        case 20:
          fiveOfKind = true;
          break;
      }
      switch (fives){
        case 10:
          twoOfKind = true;
          break;
        case 15:
          threeOfKind = true;
          break;
        case 20:
          fourOfKind = true;
          break;
        case 25:
          fiveOfKind = true;
          break;
      }
      switch (sixes){
        case 12:
          twoOfKind = true;
          break;
        case 18:
          threeOfKind = true;
          break;
        case 24:
          fourOfKind = true;
          break;
        case 30:
          fiveOfKind = true;
          break;
      }
      // integers created because boolean can't be used for what I want here which is an integer to add up totals
      int scoreTwoOfKind = 0;
      int scoreThreeOfKind = 0;
      int scoreFourOfKind = 0;
      int scoreFiveOfKind = 0;
      int scoreFullHouse = 0;
      int scoreChance = upperSectionInitialTotal;
      int scoreYahtzee = 0;
      int scoreSmallStraight = 0;
      int scoreLargeStraight = 0;
      // if statements used in case the criteria is met for each section. 
      if (threeOfKind){
        scoreThreeOfKind += upperSectionInitialTotal;
      }
      if (fourOfKind){
        scoreFourOfKind += upperSectionInitialTotal;
      }
      
      if (threeOfKind && twoOfKind){
        scoreFullHouse += 25;
      }
      
      if (fiveOfKind){
        scoreYahtzee += 50;
        
      }
      if (aces == 1 && twos == 2 && threes == 3 && fours == 4 && fives == 5){
        scoreLargeStraight = 40;
      }
      if (twos == 2 && threes == 3 && fours == 4 && fives == 5 && sixes == 6){
        scoreLargeStraight =40;
      }
      if (aces == 1 && twos == 2 && threes == 3 && fours == 4){
        scoreSmallStraight = 30;
      }
      if (twos == 2 && threes == 3 && fours == 4 && fives == 5){
        scoreSmallStraight = 30;
      }
      if (threes == 3 && fours == 4 && fives == 5 && sixes == 6){
        scoreSmallStraight = 30;
      } 
      // printing out the aquired values so it looks like the score card
      System.out.println("Three of a kind = " + scoreThreeOfKind);
      System.out.println("Four of a kind = " + scoreFourOfKind);
      System.out.println("Full House = " + scoreFullHouse);
      System.out.println("Small Straight = " + scoreSmallStraight);
      System.out.println("Large Straight = " + scoreLargeStraight);
      System.out.println("Yahtzee = " + scoreYahtzee);
      System.out.println("Chance = " + scoreChance);
      // final totals being calculated 
      int lowerSectionTotal = scoreThreeOfKind+scoreFourOfKind+scoreFullHouse+scoreYahtzee+scoreChance+scoreSmallStraight+scoreLargeStraight; 
      int grandTotal = upperSectionTotalPlusBonus+lowerSectionTotal;
      System.out.println("Total of Upper Section: " + upperSectionTotalPlusBonus);
      System.out.println("Total of Lower Section: " + lowerSectionTotal);
      System.out.println("Grand total: " + grandTotal);
    } 
}