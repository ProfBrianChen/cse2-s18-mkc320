///////
/////CSE2 Cyclometer
///Megan Carroll 2/2/18
//program is creating a bicycle cyclometer 
//
public class Cyclometer {
    //main method required for every Java program
    public static void main (String[] args) {
      int secsTrip1=480; //creates variable stating length in seconds of trip 1
      int secsTrip2=3220; //creates variable stating length in seconds of trip 2
      int countsTrip1=1561; //creates variable stating the number of counts for Trip 1
      int countsTrip2=9037; //creates variable stating the number of counts for Trip 2
      double wheelDiameter=27.0; //variable for the wheel diameter
      double PI=3.14159; //variable stating the value of PI
      int feetPerMile=5280; //variable stating the number of feet in a mile
      int inchesPerFoot=12; //variable stating the number of inches in a foot
      double secondsPerMinute=60; //variable stating the number of seconds in a minute
      double distanceTrip1; 
      double distanceTrip2; 
      double totalDistance; //creating variables with these names
      System.out.println("Trip 1 took " + (secsTrip1/secondsPerMinute) + " minutes and had " + countsTrip1 + " counts.");
      System.out.println("Trip 2 took " + (secsTrip2/secondsPerMinute) + " minutes and had " + countsTrip2 + " counts.");
      //run the calculations; store the values.
      distanceTrip1=countsTrip1*wheelDiameter*PI;
      //above gives distance in inches
      //for each count, the rotation of the wheel travels the diameter in inches times PI
      distanceTrip1/=inchesPerFoot*feetPerMile; // Gives distance in miles
      distanceTrip2=countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile;
      totalDistance=distanceTrip1+distanceTrip2;
      //print out the output data
      System.out.println("Trip 1 was "+distanceTrip1+" miles");
      System.out.println("Trip 2 was "+distanceTrip2+" miles");
      System.out.println("The total distance was "+totalDistance+" miles");
	
      
    }   //end of main method 
} //end of class 