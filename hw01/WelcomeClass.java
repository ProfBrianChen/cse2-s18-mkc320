/////////
////CSE 2 Welcome Class
///Megan Carroll 1/28/2018 
//
public class WelcomeClass {
  
    public static void main(String[] args) {
      //prints the Lehigh network ID Welcome signature and a tweet-length biography 
      /*System.out.println("  -----------");
      System.out.println("  | WELCOME |");
      System.out.println("  -----------");
      System.out.println("  ^  ^  ^  ^  ^  ^"); 
      System.out.println(" / \\/ \\/ \\/ \\/ \\/ \\ ");
      System.out.println("<-M--K--C--3--2--0->");
      System.out.println(" \\ /\\ /\\ /\\ /\\ /\\ /");
      System.out.println("  v  v  v  v  v  v");
      System.out.println("My name is Megan Carroll. I was born in Los Angeles, California but I now live in Brooklyn,NY. I am 20 years old and have one older sister.");
    */
for ( int i=6; i>0; i--){

	int val = i-2;

	if( val <= 0 ){
		System.out.println(i);
		continue;
	}

	for ( int j=0; j<val; j++) {

		for (int k=0; k<val-j; k++){
			System.out.print(i);
		}

		System.out.println();
	}

}
 
      
    }
  
}
