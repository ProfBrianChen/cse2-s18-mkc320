/////////////////////
//////CSE02 Megan Carroll
////2/13/18
///creating a rainfall calculator 
//
import java.util.Scanner;
public class Convert{
    public static void main(String []args) {
      Scanner myScanner = new Scanner( System.in );
      System.out.print("Enter the affected area in acres:"); 
      double affectedArea = myScanner.nextDouble();
      System.out.print("Enter the rainfall in the affected area in inches:");
      double rainfallArea = myScanner.nextDouble();
      affectedArea*=0.0015625;
      rainfallArea*=1.57828e-5;
      double cubicMiles = affectedArea*rainfallArea;
      System.out.println(cubicMiles + " cubic miles.");
    }
}