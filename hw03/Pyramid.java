/////////////////////
//////CSE02 Megan Carroll
////2/13/18
///creating a pyramid volume calculator 
//
import java.util.Scanner;
public class Pyramid{
    public static void main(String []args) {
      Scanner myScanner = new Scanner( System.in );
      System.out.print("The square side of the pyramid is (input length) : ");
      double squareSide = myScanner.nextDouble();
      System.out.print("The height of the pyramid is (input height) : ");
      double pyramidHeight = myScanner.nextDouble();
      double pyramidVolume = squareSide*squareSide*pyramidHeight/3; 
      System.out.println("The volume inside the pyramid " + pyramidVolume + "."); 
      
    }
}