public class RobotCity{
  public static int[][] update(int [][] cityArea){
    for(int i=0; i<cityArea.length; i++){
      for(int j=0; j<cityArea[0].length-1; j++){
       if(cityArea[i][j] < 0){
         cityArea[i][j+1] *= -1;
         cityArea[i][j] *= -1;
         j++;
         if(cityArea[i][j+1] == cityArea[0].length){
           cityArea[i][j+1] *=-1; 
         }
       }
      }  
    }
    return cityArea;
  } 
  
  public static int[][] invade(int[][] cityArea, int k){
    
    for(int i=0; i<k ; i++){
      int x,y;
      do{
        x = (int) (Math.random()*cityArea.length);
        y = (int) (Math.random()*cityArea[0].length);
      } while(cityArea[x][y]<0);
      cityArea[x][y]*= -1;
    }  
    return cityArea;
  }
    
  public static void display(int [][] cityArea){
    for(int i=0; i<cityArea.length; i++){
      for(int j=0; j<cityArea[0].length; j++){
        System.out.printf("% 4d ", cityArea[i][j]); 
      }
      System.out.println();
    }
    System.out.println();
  } 
  
  public static int[][] buildCity(){
    int [][] cityArea = new int [(int) (Math.random()*5+10)][(int) (Math.random()*5+10)];
    for(int i=0; i<cityArea.length; i++){
      for(int j=0; j<cityArea[0].length; j++){
        cityArea[i][j] = (int) (Math.random()*899+100); 
      }
    }
    return cityArea;
  }
  
   
  public static void main (String[] Args){
    int[][] cityGrid = buildCity();
    display(cityGrid);
    
    int k = (int) (Math.random()*10);
    int[][] cityGridAfterInvasion = invade(cityGrid, k);
    display(cityGridAfterInvasion);
    
    for(int i=0; i<5; i++){
      int[][] updatedCityGrid = update(cityGridAfterInvasion);
      display(updatedCityGrid);
    }
    
    
  }
}